package br.com.syonet.imobiliaria;

import java.util.List;

public class Program {

	public static void main(String[] args) {
		
		Casa casa = new Casa(1343, 20000.00, 50, 4, 15.00);
		
		casa.adicionarImovel(casa);
		
		Apartamento ap = new Apartamento(45345, 63400.00, 30, 5, false);
		
		ap.adicionarImovel(ap);
		
		Terreno terreno = new Terreno(1213, 10.000, 14000);
		
		
		terreno.adicionarImovel(terreno);
		
	
		
		List<Imovel> lista = terreno.listarImoveis();
		
		for(Imovel i: lista) {
			System.out.println(i);
		}
		

	}

}
