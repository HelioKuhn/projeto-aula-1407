package br.com.syonet.imobiliaria;

public class Casa extends Imovel{
	

	private Integer totalComodos;
	
	private Double areaTerreno;
	
	public Casa(Integer idCadasto, Double valorVenda, double area, Integer totalComodos, Double areaTerreno) {
		super(idCadasto, valorVenda, area);
		this.totalComodos = totalComodos;
		this.areaTerreno = areaTerreno;
	}

	public Integer getTotalComodos() {
		return totalComodos;
	}

	public void setTotalComodos(Integer totalComodos) {
		this.totalComodos = totalComodos;
	}

	public Double getAreaTerreno() {
		return areaTerreno;
	}

	public void setAreaTerreno(Double areaTerreno) {
		this.areaTerreno = areaTerreno;
	}
	
	public String toString() {
		return "Casa:\n" + super.toString() + "\n"
		   + "Comodos: " + totalComodos + "\n"
		+ "Área do terreno: " + areaTerreno + "\n";
	}

}
