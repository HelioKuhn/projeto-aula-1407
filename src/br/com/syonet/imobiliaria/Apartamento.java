package br.com.syonet.imobiliaria;

public class Apartamento extends Imovel{

	private Integer NumeroComodos;
	
	private Boolean possuiSalaoFestas;
	
	
	public Apartamento(Integer idCadasto, Double valorVenda, double area, Integer numeroComodos, Boolean possueSalaoFestas) {
		super(idCadasto, valorVenda, area);
		this.NumeroComodos = numeroComodos;
		this.possuiSalaoFestas = possueSalaoFestas;
	}


	public Integer getNumeroComodos() {
		return NumeroComodos;
	}


	public void setNumeroComodos(Integer numeroComodos) {
		NumeroComodos = numeroComodos;
	}


	public String getPossuiSalaoFestas() {
		String possueSalaoFestas = possuiSalaoFestas == true ? "Sim": "Não";
		return possueSalaoFestas;
	}


	public void setPossuiSalaoFestas(Boolean possuiSalaoFestas) {
		this.possuiSalaoFestas = possuiSalaoFestas;
	}
	
	public String toString() {
		return "Apartamento:\n"+ super.toString() + "\n"
		   + "Comodos: " + NumeroComodos + "\n"
		+ "Salao de festas: sim/nao: " + getPossuiSalaoFestas() + "\n";
	}

}
