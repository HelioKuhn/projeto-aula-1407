package br.com.syonet.imobiliaria;

import java.util.ArrayList;
import java.util.List;

public abstract class Imovel {
	
	private Integer idCadasto;
	
	private Double valorVenda;
	
	private double area;
	
	private static List<Imovel> listaImoveis = new ArrayList<>();

	public Imovel(Integer idCadasto, Double valorVenda, double area) {
		super();
		this.idCadasto = idCadasto;
		this.valorVenda = valorVenda;
		this.area = area;
	}

	public Integer getIdCadasto() {
		return idCadasto;
	}

	public void setIdCadasto(Integer idCadasto) {
		this.idCadasto = idCadasto;
	}

	public Double getValorVenda() {
		return valorVenda;
	}

	public void setValorVenda(Double valorVenda) {
		this.valorVenda = valorVenda;
	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}
	
	public void adicionarImovel(Imovel imovel) {
		Imovel.listaImoveis.add(imovel);
	}
	
	public List<Imovel> listarImoveis(){
		return listaImoveis;
	}
	
	public String toString() {
		return 
				 "Cadastro Imovél: " + idCadasto + "\n" +
				 "Área do Imovél: " + area + "\n"
				+ "Valor do imovel: R$" + valorVenda;
	}

}
