package br.com.syonet.imobiliaria;

public class Terreno extends Imovel {

	public Terreno(Integer idCadasto, Double valorVenda, double area) {
		super(idCadasto, valorVenda, area);
	}

	public String toString() {
		return "Terreno:\n" + super.toString() + "\n";
	}
}
