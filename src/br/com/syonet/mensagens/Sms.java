package br.com.syonet.mensagens;

public interface Sms {

	String enviaSMS(String nomeRemetente, String mensagem, String telefoneDestinatario );
}
