package br.com.syonet.mensagens;

public class EmailImpl implements Email{

	
	Contato remetente;
	
	Contato destinatario;
	
	
	public EmailImpl(Contato remetente, Contato destinatario) {
		this.remetente = remetente;
		this.destinatario = destinatario;
	}
	
	public String EnviaEmail(String emailDestinatario, String NomeDestinatario, String emailRemetente,
			String nomeRemetente, String mensagem) {
		
		if(remetente.getNome().equalsIgnoreCase(null)|| remetente.getEmail().equalsIgnoreCase(null)||remetente.getTelefone().equalsIgnoreCase(null)) {
			return null;
		}
		
		return "E-mail:\n"
				+ destinatario.getEmail()+ " - " + 
				destinatario.getNome()+ "\n" +
				remetente.getEmail() + " - " + remetente.getNome()+ "\n"
				+ mensagem + "\n"; 
	}
	
	public void setRemetente(Contato remetente) {
		this.remetente = remetente;
	}
	
	public void setDestinatario(Contato destinatario) {
		this.destinatario = destinatario;
	}
}
