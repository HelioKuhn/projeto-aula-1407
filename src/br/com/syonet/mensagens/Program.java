package br.com.syonet.mensagens;

public class Program {

	public static void main(String[] args) {
		
		Contato remetente = new Contato("Helio", "519838382", "helio@gmail.com");
		
		Contato destinatario = new Contato("João", "519458382", "joao@gmail.com");
		
		SmsImpl smsEnvia = new SmsImpl(remetente, destinatario);
		
		
		EmailImpl emailEnvia = new EmailImpl(remetente, destinatario);
		
		
		String respostaEnvioSMS = smsEnvia.enviaSMS(remetente.getNome(), "Envio de SMS", destinatario.getTelefone() );
		
		String respostaEmail = emailEnvia.EnviaEmail(destinatario.getEmail(), destinatario.getNome() , remetente.getEmail(), remetente.getNome(), "Enviando email...");
		
		System.out.println(respostaEnvioSMS);
		
		System.out.println(respostaEmail);
	}

}
