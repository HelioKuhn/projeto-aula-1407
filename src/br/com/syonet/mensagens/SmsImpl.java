package br.com.syonet.mensagens;

public class SmsImpl implements Sms {

	private Contato remetente;
	
	private Contato destinatario;
	
	
	public SmsImpl(Contato remetente, Contato destinatario) {
		this.remetente = remetente;
		this.destinatario = destinatario;
	}
	
	public String enviaSMS(String nomeRemetente, String mensagem, String telefoneDestinatario) {
		
		if(remetente.getNome().equalsIgnoreCase(null)|| remetente.getEmail().equalsIgnoreCase(null)||remetente.getTelefone().equalsIgnoreCase(null)) {
			return null;
		}
		return "SMS:\n"
				+ remetente.getNome() + " - " + mensagem + " - " + destinatario.getNome() + "\n";
		
	}
	
	public void setRemetente(Contato remetente) {
		this.remetente = remetente;
	}
	
	public void setDestinatario(Contato destinatario) {
		this.destinatario = destinatario;
	}
}
